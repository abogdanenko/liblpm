#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "lpm.h"

int
main(int argc, char **argv)
{
	unsigned long long lookups_number = 0;
	if (argc != 2 || sscanf(argv[1], "%llu", &lookups_number) != 1)
	{
		fprintf(stderr, "usage: lpmbench LOOKUPS_NUMBER\n");
		exit(EXIT_FAILURE);
	}
	printf("lookups: %llu\n", lookups_number);
	lpm_t *lpm = lpm_create();
	printf("Reading prefix to AS dataset from stdin... ");
	fflush(stdout);
	char buf[1024];
	unsigned long long record_count = 0;
	while (fgets(buf, sizeof(buf), stdin))
	{
		unsigned pref;
		unsigned as;
		uint8_t ip[4];
		sscanf(buf, "%hhu.%hhu.%hhu.%hhu %u %u", &ip[0], &ip[1], &ip[2], &ip[3], &pref, &as);
		if (lpm_insert(lpm, ip, 4, pref, (void *)(uintptr_t)as))
		{
			fprintf(stderr, "lpm_insert failed\n");
			exit(EXIT_FAILURE);
		}
		record_count++;
	}
	printf("done\n");
	printf("routes: %llu\n", record_count);
	const uint8_t unique_ip_addresses = 100;
	printf("unique_ip_addresses: %hhu\n", unique_ip_addresses);
	const int batch_size = 100;
	printf("batch_size: %d\n", batch_size);
	unsigned long long lookup_count = 0;
	unsigned long long checksum = 0;
	printf("Performing lookups... ");
	fflush(stdout);
	const clock_t time_before = clock();
	while (lookup_count < lookups_number)
	{
		for (uint8_t i = 1; i <= unique_ip_addresses && lookup_count < lookups_number; i++)
		{
			const uint8_t ip[4] = {i, i, i, i};
			for (int j = 0; j < batch_size && lookup_count < lookups_number; j++)
			{
				void *res = lpm_lookup(lpm, ip, 4);
				lookup_count++;
				checksum += (uintptr_t) res;
			}
		}
	}
	const clock_t time_after = clock();
	const double duration = ((double) (time_after - time_before)) / CLOCKS_PER_SEC;
	printf("done\n");
	printf("duration: %f seconds\n", duration);
	printf("one lookup: %.2f nanoseconds\n", 1.0E+9 * duration / lookups_number);
	printf("lookups per second: %.2f\n", lookups_number / duration);
	printf("checksum: %llu\n", checksum);
	lpm_destroy(lpm);
	return EXIT_SUCCESS;
}

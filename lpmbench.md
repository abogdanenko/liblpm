lpmbench
========

Longest Prefix Match (LPM) benchmark tool.

Measure LPM lookups per second for a given IPv4 Prefix-to-Autonomous System (AS)
database.

Quickstart
----------

```sh
cd src
gcc -std=c99 -O3 -o lpmbench lpm.c lpmbench.c
wget https://abogdanenko.gitlab.io/liblpm/routeviews-rv2-20201001-2000.pfx2as
./lpmbench 10000000 < routeviews-rv2-20201001-2000.pfx2as
```

Output Example:

```
lookups: 10000000
Reading prefix to AS dataset from stdin... done
routes: 881575
unique_ip_addresses: 100
batch_size: 100
Performing lookups... done
duration: 2.540831 seconds
one lookup: 254.08 nanoseconds
lookups per second: 3935720.24
checksum: 133829100000
```

Usage
-----

```
usage: lpmbench LOOKUPS_NUMBER
```

Prefix-to-AS database is read from stdin. For example:

```
$ ./lpmbench 10000000 <<EOF
1.1.1.0 24 7773
8.8.8.0 24 12345
EOF
lookups: 10000000
Reading prefix to AS dataset from stdin... done
routes: 2
unique_ip_addresses: 100
batch_size: 100
Performing lookups... done
duration: 0.177775 seconds
one lookup: 17.78 nanoseconds
lookups per second: 56250878.92
checksum: 2011800000
```

The file format is line-oriented, with one prefix-AS mapping per line. The
tab-separated fields are

1. IP prefix
2. prefix length
3. AS

The AS can be a single AS number, or a multi-origin AS (e.g. 10_20 ).

The database format is described at

"Routeviews Prefix to AS mappings Dataset (pfx2as) for IPv4 and IPv6"
https://www.caida.org/data/routing/routeviews-prefix2as.xml

Database files can be downloaded from the following location
http://data.caida.org/datasets/routing/routeviews-prefix2as/

Example database file (compressed):
http://data.caida.org/datasets/routing/routeviews-prefix2as/2020/10/routeviews-rv2-20201001-2000.pfx2as.gz

Mirror: https://abogdanenko.gitlab.io/liblpm/routeviews-rv2-20201001-2000.pfx2as

The tool performs LPM lookup of the following IP addresses:

```
1.1.1.1, 1.1.1.1, 1.1.1.1, ..., 1.1.1.1,
2.2.2.2, 2.2.2.2, 2.2.2.2, ..., 2.2.2.2,

<-------- batch size = 100 ------------>
...

100.100.100.100, 100.100.100.100, 100.100.100.100, 100.100.100.100
```

until a total of `LOOKUPS_NUMBER` is reached.
